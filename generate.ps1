if (!(Test-Path "bin")) {
	New-Item "bin" -type directory
} else {
	Remove-Item "bin\*" -Recurse
}

Copy-Item -Path "index.html" "bin\index.html"
$langs = "ru","en"
foreach($lang in $langs){

	Copy-Item -Path "css\" -Recurse ("bin\" + $lang + "\css\")
	Copy-Item -Path "fonts\" -Recurse ("bin\" + $lang + "\fonts\")
	Copy-Item -Path "js\" -Recurse ("bin\" + $lang + "\js\")
	Copy-Item -Path "images\" -Recurse ("bin\" + $lang + "\images\")

	$result1 = Get-ChildItem -path ($lang + "\page")
	$index_tpl = Get-Content -path ($lang + "\index.html") -Encoding UTF8

	foreach($t in $result1){

		$g = Get-Content -Encoding utf8 -path ($lang +"\page\"  + $t.name) 
		$res = New-Object -typeName System.Collections.ArrayList
		foreach ($tree in $index_tpl) {
			if ( $tree.IndexOf("{{content}}") -gt 0  ){
				$i= $res.AddRange($g)
			}else{
				$i=  $res.Add($tree)
			}
		}

		set-content -path ("bin\" + $lang + "\" + $t.name) -value $res -Encoding utf8
		echo ( $lang + "`t done ->`t" +$t.name)
	}
	echo ( " ")
}

#